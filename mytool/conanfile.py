from conans import ConanFile, tools
import os

class MytoolConan(ConanFile):
    name = "mytool"
    version = "1.0"
    settings = "os"

    def package(self):
        self.copy("*")

    def package_info(self):
        bindir = os.path.join(self.package_folder, "bin")
        self.output.info("Appending PATH environment variable: {}".format(bindir))
        self.env_info.PATH.append(bindir)
