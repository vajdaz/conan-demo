from conans import ConanFile, AutoToolsBuildEnvironment
from conans import tools

class AppConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "mylib/1.0"
    generators = "make", "virtualrunenv"

    def build(self):
        atools = AutoToolsBuildEnvironment(self)
        atools.make()
