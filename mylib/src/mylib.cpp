#include <iostream>
#include "mylib.h"

void mylib_function() {
    #ifdef NDEBUG
    std::cout << "Implemented in release version of mylib.so!" <<std::endl;
    #else
    std::cout << "Implemented in debug version of mylib.so!" <<std::endl;
    #endif
}
